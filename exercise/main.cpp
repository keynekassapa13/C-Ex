//
//  main.cpp
//  exercise
//
//  Created by Keyne Kassapa on 11/7/18.
//  Copyright © 2018 Keyne Kassapa. All rights reserved.
//

#include <iostream>

using namespace std; //make names from std visible

double square(double x){ //square a double precision
    return x*x;
}

void print_square(double x){
    cout << "the square of " << x << " is " << square(x) << "\n";
}

int main() {
    print_square(1.234);
}

int mainhelloworld(int argc, const char * argv[]) {
    // insert code here...
    std::cout << "Hello, World!\n";
    return 0;
}
